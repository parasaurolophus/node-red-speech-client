Copyright &copy; 2020 Kirk Rader

# speech-client

![](./kiosk.jpg)

Speech capture for 100% cloud-free voice assistance in a home automation network using [node-red-contrib-voice2json][].

This flow listens for the wake word, records the subsequent utterance and sends it to the speech server via MQTT.

![](./speech-client-flow.png)

## Overview

This flow is one part of a comprehensive solution providing voice assistant functionality to a home automation system without relying on any "cloud" based services such as those from Google, Amazon, Apple, Microsoft et al. It interoperates with services provided by speech processing and home automation Node-RED flows, described separately.

```mermaid
flowchart TD

  subgraph "Front End (one per room)"
    client[node-red-speech-client]
    microphone([Mic Array /<br/>Speaker])
  end

  subgraph "Back End (one per home)"
    server[node-red-speech-server]
    automation[node-red-intent-automation]
    broker[MQTT Broker]
  end

  subgraph "Local IoT Networks"
    devices[Various Devices]
  end

  client --> | 1. wait for wake phrase<br/>5. record utterance | client
  microphone --> | 2. wake phrase<br/>4. utterance | client
  client --> | 3. feedback sound<br/>6. feedback sound | microphone
  client --> | 7. recording | broker
  broker --> | 7. recording | server
  server --> | 8. Speech-to-Text<br/>9. Text-to-Intent | server
  server --> | 10. intent | broker
  broker --> | 10. intent | automation
  automation <--> | 11. monitoring and control | devices
  automation <--> | 12. dashboard | client

  classDef this text:black,fill:pink
  class client this
```

All communication between speech services is via MQTT. Device control uses various protocols including TCP/IP (WiFi or Bluetooth), Z-Wave, Zigbee etc.

- See <https://gitlab.com/parasaurolophus/node-red-speech-server> for the implementation of [node-red-speech-server][].

- See <https://gitlab.com/parasaurolophus/node-red-intent-automation> for an example of [node-red-intent-automation][].

## Dependencies

- [voice2json](http://voice2json.org)
    - See <http://voice2json.org/>

- Compatible audio hardware
    - Microphone suitable for far-field voice capture and recognition[^microphone]
    - Speaker to emit feedback sounds

- [node-red-contrib-voice2json][]
    - Install via `npm` as per the instructions at <https://github.com/johanneskropf/node-red-contrib-voice2json>

- [node-red-contrib-sox-utils](https://github.com/johanneskropf/node-red-contrib-sox-utils)
    - Automatically installed as explicit dependency by _node-red-contrib-voice2json_

- [node-red-contrib-fan](https://flows.nodered.org/node/node-red-contrib-fan)

- MQTT broker

_Note: You can use a public, shared MQTT broker but to be truly "cloud free" you need to host a broker inside your LAN, e.g. using [Mosquitto](https://mosquitto.org). Conversely, if your broker can be securely accessed over the Internet then your voice capture clients do not need even to be in the same time zone as your voice processing server. This opens up interesting possibilities for "personal cloud," multiple-dwelling-unit and similar scenarios._

## Installation

1. Connect a suitable microphone and speaker to the machine that will run this Node-RED flow

2. Follow the instructions at <http://voice2json.org/install.html> to install _voice2json_ on the same machine as Node-RED

3. Follow the instructions at <https://github.com/johanneskropf/node-red-contrib-voice2json> to install _node-red-contrib-voice2json_

    - Be sure to download and unpack a speech profile into somewhere Node-RED can access as per the instructions

4. (Optional) Follow the [tutorial](https://github.com/johanneskropf/node-red-contrib-voice2json/wiki/Getting-started---Step-by-step-tutorial) to verify that you have everything set up correctly before importing the flow described below

5. Import [flow.json](./flow.json) into Node-RED

6. Edit the MQTT broker configuration nodes to match your setup

7. Place a WAV file you wish to use as a wake-word acknowledgment tone in a directory accessible to Node-RED

8. Edit the pathname in the `change` node that feeds the `sox play` node to reference the WAV file

9. Edit the _voice2json_ configuration node to reference your downloaded speech profile

10. Click the `inject` node named _train_

## Usage

Once you have completed all of the preceding [Installation](#installation) steps, wake-word processing and voice capture will be continuously operating whenever this Node-RED flow is running.

```mermaid
sequenceDiagram

  participant user as User
  participant client as Speech<br/>Client
  participant broker as MQTT<br/>Broker

  loop forever
    client ->> client: wait for wake phrase
    user ->> client: say wake phrase
    activate client
    client ->> user: play feedback sound
    client ->> client: start recording
    loop client keeps recording until silence detected or timeout
      user ->> client: say command
    end
    client ->> client: stop recording
    client ->> user: play feedback sound
    opt successful recording?
      client ->> broker: send recording
    end
    deactivate client
  end
```

Saying "Hey Mycroft" should result in your hearing your chosen feedback sound played. Anything you say soon after that will be captured and sent as a MQTT message, presumably for processing by the speech server alluded to near the top of this _README.md_.

If all goes as expected, the captured audio stream will be sent as a MQTT message to be acted upon by the the speech server. Ultimately, the speech server should send a corresponding intent message which is displayed by this flow for debugging purposes.

Note that it requires some practice to interact with _voice2json_ successfully. Common issues include:

- Failure to wait for the acknowledgment tone after saying the wake phrase and before saying the spoken command

- Waiting too long to start saying the command after hearing the acknowledgment tone so that the recording attempt times out

- Not pausing long enough after saying the command so that too long an utterance is recorded

In addition, the type of microphone, its placement and the background sound environment will all affect the success rate of speech recognition.

Note also that while the preceding sequence diagram indicates that the speech client records a "command" spoken by a user, in reality this is just any spoken utterance that follows an utterance of the wake phrase and terminated by a pause in speech. The speech client sends that recording as a packet of data in WAV format to the MQTT broker and resumes waiting for the next utterance of the wake phrase. It is up to other subsystems to interpret that recorded utterance ([node-red-speech-server][]) and carry out the user's intended command. ([node-red-intent-automation][]).

[^microphone]: Some users of _voice2json_ report success using very common, general purpose microphones. My own personal experience is that for an acceptable replacement for products like Alexa or Google Assistant a more specialized device optimized for far-field voice capture and processing is required. These are harder to find and more expensive than the typical mic aimed at desktop users. For example, I have had good luck with the USB-based [ReSpeaker Mic Array v2.0](https://www.seeedstudio.com/ReSpeaker-Mic-Array-v2-0.html) and pretty abysmal voice-capture performance from any other mic I have tried, including the more modestly priced 2-mic Pi HAT version from the same manufacturer as the _ReSpeaker_ 4-mic array. The area where a more sophisticated mic with built in signal processing helps is in distinguishing speech from background noise. The speech capture and speech-to-text nodes can be easily defeated when there is even very moderate noise competing with the speech, such as that created by the fans in heatering and air conditioning systems, let alone other sounds from others sources like music or TV. Using basic microphones, the speech capture node may never hear what it considers "silence" so as to terminate recording and send an utterance to be converted to text in contexts where a device like the _ReSpeaker_ mic array can do so. Where such a device has its own built in speaker or an analog audio output jack, you should use that as the output device in the SOX utilities output node. This will interoperate correctly with the mic's built in signal processing to improve overall performance and accuracy.

[node-red-contrib-voice2json]: https://github.com/johanneskropf/node-red-contrib-voice2json
[node-red-speech-server]: https://gitlab.com/parasaurolophus/node-red-speech-server
[node-red-intent-automation]: https://gitlab.com/parasaurolophus/node-red-intent-automation
